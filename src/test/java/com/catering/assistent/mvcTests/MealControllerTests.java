package com.catering.assistent.mvcTests;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.catering.assistent.business.MealServiceImpl;
import com.catering.assistent.controller.MealController;
import com.catering.assistent.model.Meal;
import com.catering.assistent.model.response.ResponseGetMeal;

import org.junit.*;
import org.junit.runner.*;
import org.skyscreamer.jsonassert.JSONAssert;
import org.springframework.beans.factory.annotation.*;
import org.springframework.boot.test.mock.mockito.*;
import org.springframework.test.context.junit4.*;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.hamcrest.Matchers.*;

@RunWith(SpringRunner.class)
@WebMvcTest(value = MealController.class, secure = false)
public class MealControllerTests {
	@MockBean
	MealServiceImpl mealServiceImpl;

	@Autowired
	MockMvc mvc;

	Meal meal1 = new Meal("sarme");
	Meal meal2 = new Meal("sarme sa kupusom");
	ResponseGetMeal responseGetMeal1 = new ResponseGetMeal(meal1.getMealName());
	ResponseGetMeal responseGetMeal2 = new ResponseGetMeal(meal2.getMealName());

	List<ResponseGetMeal> allMeals = new ArrayList<> (Arrays.asList(responseGetMeal1,responseGetMeal2));

	@Test
	public void returnAllMeals() throws Exception {
		
		when(mealServiceImpl.returnAllMealsService()).thenReturn(allMeals);

		mvc.perform(get("/meal/all").contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk())
				.andExpect(jsonPath("$.*", hasSize(2)))
				.andExpect(jsonPath("$[0].mealName", is(responseGetMeal1.getMealName())));
	}

	@Test
	public void returnAllMealsWithRequestBuider() throws Exception {
		
		when(mealServiceImpl.returnAllMealsService()).thenReturn(allMeals);
		
		RequestBuilder requestBuilder = MockMvcRequestBuilders.get("/meal/all").accept(MediaType.APPLICATION_JSON);
		MvcResult result = mvc.perform(requestBuilder).andReturn();

		String expected = "[{\"mealName\":\"sarme\"},{\"mealName\":\"sarme sa kupusom\"}]";
		
		JSONAssert.assertEquals(expected, result.getResponse().getContentAsString(), false);
	}
}