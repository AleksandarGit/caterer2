package com.catering.assistent.serviceTests;

import static org.junit.Assert.assertEquals;

import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import com.catering.assistent.business.MealServiceImpl;
import com.catering.assistent.exception.MyResourceNotFoundException;
import com.catering.assistent.model.Meal;
import com.catering.assistent.model.response.ResponseGetMeal;
import com.catering.assistent.repository.MealRepository;
import static org.mockito.BDDMockito.*;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SpringRunnerTests {

	@MockBean
	private MealRepository mealRepository;

	@Autowired
	private MealServiceImpl mealServiceImpl;

	
	private Meal meal= new Meal("sarme");
	private Long id = 1L;
	private ResponseGetMeal responseGetMeal = new ResponseGetMeal(meal.getMealName());

	@Test
	public void testReturnMealById() throws MyResourceNotFoundException {
		// prepare
		given(mealRepository.findById(id)).willReturn(Optional.of(meal));
		ResponseGetMeal expected = responseGetMeal;
		// execute
		ResponseGetMeal actual = mealServiceImpl.returnMealById(id);
		// assert
		assertEquals(actual.getMealName(), expected.getMealName());
	}

}
