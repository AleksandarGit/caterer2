package com.catering.assistent.serviceTests;

import com.catering.assistent.business.MealServiceImpl;
import com.catering.assistent.model.Meal;
import com.catering.assistent.model.request.RequestPostMeal;
import com.catering.assistent.model.response.ResponseGetMeal;
import com.catering.assistent.repository.MealRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verifyNoMoreInteractions;

@RunWith(MockitoJUnitRunner.class)
public class MealServiceTests {

	@Mock
	private MealRepository mealRepository;

	@InjectMocks
	private MealServiceImpl mealServiceImpl;
	private Long id = 1L;
	private Meal meal = new Meal("sarme");
	
	
	private ResponseGetMeal responseGetMeal = new ResponseGetMeal(meal.getMealName());
	private RequestPostMeal requestPostMeal = new RequestPostMeal(meal.getMealName());

	@Test
	public void testReturnAllMealsService() {
		// prepare
		List<Meal> mealList = new ArrayList<>();
		mealList.add(meal);
		List<ResponseGetMeal> expected = new ArrayList<>();
		expected.add(responseGetMeal);
		Mockito.when(mealRepository.findAll()).thenReturn(mealList);
		// execute
		List<ResponseGetMeal> actual = mealServiceImpl.returnAllMealsService();
		// assert
		assertEquals(actual.get(0).getMealName(), expected.get(0).getMealName());
	}

	// public ResponseGetMeal returnMealById(Long id)
	@Test
	public void testReturnMealById(){
		// prepare
		Mockito.when(mealRepository.findById(id)).thenReturn(Optional.of(meal));
		ResponseGetMeal expected = responseGetMeal;
		// execute
		ResponseGetMeal actual = mealServiceImpl.returnMealById(id);
		// assert
		assertEquals(actual.getMealName(), expected.getMealName());
	}

	// public ResponseGetMeal returnMealByMealName(String mealName) 
	@Test
	public void testReturnMealByMealName(){
		// prepare
		Mockito.when(mealRepository.findByMealName(meal.getMealName())).thenReturn(Optional.of(meal));
		ResponseGetMeal expected = responseGetMeal;
		// execute
		ResponseGetMeal actual = mealServiceImpl.returnMealByMealName(meal.getMealName());
		// assert
		assertEquals(actual.getMealName(), expected.getMealName());
	}

	// public void deleteMealById(Long id)
	@Test
	public void testDeleteMealById(){
		// execute
		mealServiceImpl.deleteMealById(id);
		// assert
		Mockito.verify(mealRepository, times(1)).deleteById(id);
	}

	// public ResponseGetMeal saveMeal(RequestPostMeal requestPostMeal)
	@Test
	public void testSaveMeal() {
		// prepare
		ResponseGetMeal expected = responseGetMeal;
		// execute
		ResponseGetMeal actual = mealServiceImpl.saveMeal(requestPostMeal);
		// assert
		Mockito.verify(mealRepository, times(1)).save(any(Meal.class));
		verifyNoMoreInteractions(mealRepository);
		assertEquals(actual.getMealName(), expected.getMealName());

	}

	// public ResponseGetMeal updateMeal(Long id, RequestPostMeal requestPostMeal)
	
	@Test
	public void testUpdateMeal(){
		// prepare
		ResponseGetMeal expected = responseGetMeal;
		Mockito.when(mealRepository.findById(id)).thenReturn(Optional.of(meal));
		// execute
		ResponseGetMeal actual = mealServiceImpl.updateMeal(id, requestPostMeal);
		// assert
		assertEquals(actual.getMealName(), expected.getMealName());
	}
}
