package com.catering.assistent.model;


import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.Data;
import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;


@Entity(name = "Meal")
@Table(name = "meal")
@Data
public class Meal {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "mealId")
    private long id;

    @Column(nullable = false)
    private String mealName;

    @OneToMany(
            mappedBy = "meal",
            cascade = CascadeType.ALL,
            orphanRemoval = true
    )
    @JsonManagedReference
    private List<OrderFromUser> orderFromUsers = new ArrayList<>();
    public Meal() {
    }

    public Meal(String mealName) {
        this.mealName = mealName;
    }
    public Meal(long id, String mealName) {
        this.id = id;
        this.mealName = mealName;
    }

}
