package com.catering.assistent.model;


import javax.persistence.*;
import com.fasterxml.jackson.annotation.JsonBackReference;

import lombok.Builder;
import lombok.Data;
import java.time.LocalDateTime;

@Entity(name = "OrderFromUser")
@Table(name = "orderFromUser")
@Data
@Builder
public class OrderFromUser {


    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long mealUserId;

    @JsonBackReference
    @ManyToOne(fetch = FetchType.EAGER)
    private Meal meal;

    @JsonBackReference
    @ManyToOne(fetch = FetchType.EAGER)
    private User user;

    @Column(name = "date")
    private LocalDateTime date;

    public OrderFromUser() {
    }

    public OrderFromUser(Meal meal, User user) {
        this.meal = meal;
        this.user = user;

    }
    public OrderFromUser(Meal meal, User user, LocalDateTime date) {
        this.meal = meal;
        this.user = user;
        this.date = date;

    }

    public OrderFromUser(Long mealUserId, Meal meal, User user, LocalDateTime date) {
        this.mealUserId = mealUserId;
        this.meal = meal;
        this.user = user;
        this.date = date;
    }

  
}