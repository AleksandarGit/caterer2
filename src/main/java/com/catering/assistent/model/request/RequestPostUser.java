package com.catering.assistent.model.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class RequestPostUser {
	@NotNull
	private String userFirstName;
	@NotNull
	private String userLastName;
	@NotNull
	@Email
	private String userEmail;
}
