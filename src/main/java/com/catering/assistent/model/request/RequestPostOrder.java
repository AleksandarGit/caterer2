package com.catering.assistent.model.request;


import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class RequestPostOrder {
    @NotNull
    private String userEmail;
    @NotNull
    private String mealName;

}
