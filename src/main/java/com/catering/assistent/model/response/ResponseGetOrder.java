package com.catering.assistent.model.response;

import com.catering.assistent.model.OrderFromUser;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ResponseGetOrder {
	private String userFirstName;
	private String userLastName;
	private String mealName;
	private String date;
	private long id;

	public ResponseGetOrder(OrderFromUser orderFromUser) {
		this.userFirstName = orderFromUser.getUser().getFirstName();
		this.userLastName = orderFromUser.getUser().getLastName();
		this.mealName = orderFromUser.getMeal().getMealName();
		this.date = orderFromUser.getDate().toString();
		this.id = orderFromUser.getMealUserId();
	}

}
