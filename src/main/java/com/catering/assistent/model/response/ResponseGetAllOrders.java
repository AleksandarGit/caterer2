package com.catering.assistent.model.response;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ResponseGetAllOrders {
    private String datum;
    private List<ResponseGetOrder> orderList;
}
