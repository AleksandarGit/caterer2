package com.catering.assistent.model.response;

import com.catering.assistent.model.User;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ResponseGetUser {
	private String userFirstName;
	private String userLastName;
	private String userEmail;

	public ResponseGetUser(User user) {
		this.userFirstName = user.getFirstName();
		this.userLastName = user.getLastName();
		this.userEmail = user.getEmail();
	}
}
