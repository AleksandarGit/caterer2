package com.catering.assistent.model;

import com.catering.assistent.model.request.RequestPostUser;
import com.catering.assistent.util.ExtendedEmailValidator;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.Email;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

@Entity(name = "User")
@Table(name = "users")
@Data
public class User {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "userId")
	private long id;
	@Column(nullable = true)
	private String role;
	@Column(nullable = false)
	private String firstName;
	@Column(nullable = false)
	private String lastName;
	@Column(nullable = false)
	@ExtendedEmailValidator
	private String email;

	@OneToMany(mappedBy = "user", cascade = CascadeType.ALL, orphanRemoval = true)
	@JsonManagedReference
	private List<OrderFromUser> orderFromUsers = new ArrayList<>();

	public void addMeal(Meal meal) {
		OrderFromUser orderFromUser = new OrderFromUser(meal, this);
		orderFromUsers.add(orderFromUser);
		meal.getOrderFromUsers().add(orderFromUser);
	}

	public void removeMeal(Meal meal) {
		for (Iterator<OrderFromUser> iterator = orderFromUsers.iterator(); iterator.hasNext();) {
			OrderFromUser orderFromUser = iterator.next();

			if (orderFromUser.getUser().equals(this) && orderFromUser.getMeal().equals(meal)) {
				iterator.remove();
				orderFromUser.getMeal().getOrderFromUsers().remove(orderFromUser);
				orderFromUser.setUser(null);
				orderFromUser.setMeal(null);
			}
		}
	}

	public User() {
	}

	public User(String role, String firstName, String lastName, @Email String email) {
		this.role = role;
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
	}

	public User(String firstName, String lastName, @Email String email) {
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
	}

	public User(long id, String role, String firstName, String lastName, @Email String email) {
		this.id = id;
		this.role = role;
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
	}
	public User(long id, String firstName, String lastName, @Email String email) {
		this.id = id;
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
	}

	public User(RequestPostUser req) {
		this.firstName = req.getUserFirstName();
		this.lastName = req.getUserLastName();
		this.email = req.getUserEmail();
	}

}
