package com.catering.assistent.business;

import com.catering.assistent.exception.DataMissingException;
import com.catering.assistent.exception.MyResourceNotFoundException;
import com.catering.assistent.model.Meal;
import com.catering.assistent.model.OrderFromUser;
import com.catering.assistent.model.User;
import com.catering.assistent.model.request.RequestPostOrder;
import com.catering.assistent.model.response.*;
import com.catering.assistent.repository.MealRepository;
import com.catering.assistent.repository.OrderRepository;
import com.catering.assistent.repository.UserRepository;

import lombok.AllArgsConstructor;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import static com.catering.assistent.util.EmailValidation.validateEmailApache;
import static com.catering.assistent.util.StringDateConverter.returnTwoAdjecentDays;

@Service
@Transactional
@AllArgsConstructor
public class OrderServiceImpl implements OrderService {

	private final OrderRepository orderRepository;
	private final MealRepository mealRepository;
	private final UserRepository userRepository;


	@Override
	public ResponseGetOrder returnOrderById(Long id) throws MyResourceNotFoundException{
		Optional<OrderFromUser> orderById = orderRepository.findById(id);
		if (!orderById.isPresent()) {
			throw new MyResourceNotFoundException("Order not found!", id);
		}
		OrderFromUser orderFromUser = orderById.get();
		return new ResponseGetOrder(orderFromUser);
	}

	@Override
	public List<ResponseGetOrder> returnAllOrderService() throws MyResourceNotFoundException {
		List<OrderFromUser> lista = (List<OrderFromUser>) orderRepository.findAll();
		
		List<ResponseGetOrder> responseOrderList = new ArrayList<>();

		lista.forEach(item -> responseOrderList.add(new ResponseGetOrder(item)));

		return responseOrderList;

	}

	@Override
	public void deleteOrderById(Long id) throws MyResourceNotFoundException {
		Optional<com.catering.assistent.model.OrderFromUser> orderById = orderRepository.findById(id);
		if (!orderById.isPresent()) {
			throw new MyResourceNotFoundException("Order not found!", id);
		}
		orderRepository.deleteById(id);
	}

	@Override
	public ResponseGetOrder saveOrder(@Valid RequestPostOrder requestPostOrder)
			throws DataMissingException, MyResourceNotFoundException {
		
		Optional<Meal> opMeal = mealRepository.findByMealName(requestPostOrder.getMealName());
		
		Optional<User> opUser = userRepository.findByEmail(requestPostOrder.getUserEmail());
		
		OrderFromUser orderFromUser = new OrderFromUser();
		orderFromUser.setMeal(opMeal.get());
		orderFromUser.setUser(opUser.get());
		orderFromUser.setDate(LocalDateTime.now());
		
		OrderFromUser response = orderRepository.save(orderFromUser);
		ResponseGetOrder responseGetOrder = new ResponseGetOrder(response);
		
		return responseGetOrder;
	}

	@Override
	public ResponseGetOrder updateOrder(Long id, @Valid RequestPostOrder requestPostOrder)
			throws DataMissingException, MyResourceNotFoundException {

		Optional<OrderFromUser> opOrderFromUser = orderRepository.findById(id);
		if (!opOrderFromUser.isPresent()) {
			throw new MyResourceNotFoundException("Order not found!", id);
		}
		Optional<Meal> opMeal = mealRepository.findByMealName(requestPostOrder.getMealName());
		if (!opMeal.isPresent()) {
			throw new MyResourceNotFoundException("Meal not found!",id);
		}
		Optional<User> opUser = userRepository.findByEmail(requestPostOrder.getUserEmail());
		if (!opUser.isPresent()) {
			throw new MyResourceNotFoundException("User not found!",id);
		}
		OrderFromUser orderFromUser = opOrderFromUser.get();
		orderFromUser.setMeal(opMeal.get());
		orderFromUser.setUser(opUser.get());
		orderFromUser.setDate(LocalDateTime.now());

		orderRepository.save(orderFromUser);
		return new ResponseGetOrder(orderFromUser);
	}

	@Override
	public ResponseGetAllOrdersByEmail returnOrderByUserEmail(String email)
			throws MyResourceNotFoundException, DataMissingException {
		
		validateEmailApache(email);
		List<OrderFromUser> orderByEmail = orderRepository.findByEmail(email);

		List<ResponseGetOrder> responseOrderList = new ArrayList<>();

		orderByEmail.forEach(item -> responseOrderList.add(new ResponseGetOrder(item)));

		User userByEmail = userRepository.findByEmail(email).get();
		ResponseGetAllOrdersByEmail responseGetAllOrdersByEmail = new ResponseGetAllOrdersByEmail();
		responseGetAllOrdersByEmail.setOrderList(responseOrderList);
		responseGetAllOrdersByEmail.setUserName(
				userByEmail.getFirstName() + " " + userByEmail.getLastName() + " " + userByEmail.getEmail());

		return responseGetAllOrdersByEmail;
	}

	@Override
	public ResponseGetAllOrders returnOrderByDate(String dateString)
			throws MyResourceNotFoundException, DataMissingException {

		LocalDateTime[] dateTimeObjs = returnTwoAdjecentDays(dateString);

		List<OrderFromUser> orderByDate = orderRepository.findByDate(dateTimeObjs[0], dateTimeObjs[1]);

		List<ResponseGetOrder> responseOrderList = new ArrayList<>();

		orderByDate.forEach(item -> responseOrderList.add(new ResponseGetOrder(item)));

		ResponseGetAllOrders responseGetAllOrdersByDate = new ResponseGetAllOrders();
		responseGetAllOrdersByDate.setDatum(dateString);
		responseGetAllOrdersByDate.setOrderList(responseOrderList);
		return responseGetAllOrdersByDate;
	}

	@Override
	public ResponseGetAllOrders returnOrderByMeal(String meal)
			throws MyResourceNotFoundException, DataMissingException {
		
		List<OrderFromUser> orderByMeal = orderRepository.findByMeal(meal);
		List<ResponseGetOrder> responseOrderList = new ArrayList<>();

		orderByMeal.forEach(item -> responseOrderList.add(new ResponseGetOrder(item)));
		
		ResponseGetAllOrders responseGetAllOrdersByMeal = new ResponseGetAllOrders();
		responseGetAllOrdersByMeal.setOrderList(responseOrderList);
		return responseGetAllOrdersByMeal;

	}

	@Override
	public ResponseGetOrder saveOrderWithDate(String date, @Valid RequestPostOrder requestPostOrder)
			throws DataMissingException, MyResourceNotFoundException {
		
		Optional<Meal> opMeal = mealRepository.findByMealName(requestPostOrder.getMealName());
		
		Optional<User> opUser = userRepository.findByEmail(requestPostOrder.getUserEmail());
		
		LocalDateTime dateObj1 = returnTwoAdjecentDays(date)[0];

		OrderFromUser orderFromUser = new OrderFromUser();
		orderFromUser.setMeal(opMeal.get());
		orderFromUser.setUser(opUser.get());
		orderFromUser.setDate(dateObj1);

		orderRepository.save(orderFromUser);
		ResponseGetOrder responseGetOrder = new ResponseGetOrder();
		responseGetOrder.setUserLastName(orderFromUser.getUser().getLastName());
		responseGetOrder.setUserFirstName(orderFromUser.getUser().getFirstName());
		responseGetOrder.setMealName(orderFromUser.getMeal().getMealName());
		responseGetOrder.setDate(orderFromUser.getDate().toString());

		return responseGetOrder;

	}

}
