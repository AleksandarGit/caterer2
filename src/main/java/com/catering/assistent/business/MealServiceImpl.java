package com.catering.assistent.business;

import com.catering.assistent.model.Meal;
import com.catering.assistent.model.request.RequestPostMeal;
import com.catering.assistent.model.response.ResponseGetMeal;
import com.catering.assistent.repository.MealRepository;

import lombok.AllArgsConstructor;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

@Service
@Transactional
@AllArgsConstructor
public class MealServiceImpl implements MealService {

    private final MealRepository mealRepository;
  

    @Override
    public List<ResponseGetMeal> returnAllMealsService() {
        List<Meal> lista = (List<Meal>) mealRepository.findAll();
        List<ResponseGetMeal> responseMealList = new ArrayList<>();

        lista.forEach(item -> responseMealList.add(new ResponseGetMeal(item.getMealName())));

        return responseMealList;
    }

    @Override
    public ResponseGetMeal returnMealById(Long id){
        Optional<Meal> mealById = mealRepository.findById(id);
        
        ResponseGetMeal responseGetMealById = new ResponseGetMeal();
        responseGetMealById.setMealName(mealById.get().getMealName());
        return responseGetMealById;
    }

    public ResponseGetMeal returnMealByMealName(String mealName){
        Optional<Meal> mealByMealName = mealRepository.findByMealName(mealName);
        
        ResponseGetMeal responseGetMealById = new ResponseGetMeal();
        responseGetMealById.setMealName(mealByMealName.get().getMealName());
        return responseGetMealById;
    }

    @Override
    public void deleteMealById(Long id){        
        mealRepository.deleteById(id);

    }

    @Override
    public ResponseGetMeal saveMeal(@Valid RequestPostMeal requestPostMeal) {
    	
        Meal request = new Meal(requestPostMeal.getMealName());
        mealRepository.save(request);


        ResponseGetMeal responseGetMeal = new ResponseGetMeal();
        responseGetMeal.setMealName(request.getMealName());
        return responseGetMeal;
    }

    @Override
    public ResponseGetMeal updateMeal(Long id,@Valid RequestPostMeal requestPostMeal) {
        
        Optional<Meal> mealById = mealRepository.findById(id);
        
        
        mealById.get().setMealName(requestPostMeal.getMealName());

        ResponseGetMeal responseGetMeal = new ResponseGetMeal();
        responseGetMeal.setMealName(mealById.get().getMealName());
        return responseGetMeal;
    }
}
