package com.catering.assistent.business;

import com.catering.assistent.model.request.RequestPostMeal;
import com.catering.assistent.model.response.ResponseGetMeal;

import java.util.List;

import javax.validation.Valid;

public interface MealService {
    List<ResponseGetMeal> returnAllMealsService();

    ResponseGetMeal returnMealById(Long id);

    void deleteMealById(Long id);

    ResponseGetMeal saveMeal(@Valid RequestPostMeal requestPostMeal);

    ResponseGetMeal updateMeal(Long id,@Valid RequestPostMeal requestPostMeal);

    ResponseGetMeal returnMealByMealName(String mealName);

}
