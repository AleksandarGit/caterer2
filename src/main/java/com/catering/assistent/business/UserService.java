package com.catering.assistent.business;

import com.catering.assistent.exception.DataMissingException;
import com.catering.assistent.exception.EmailExistentException;
import com.catering.assistent.exception.MyResourceNotFoundException;
import com.catering.assistent.model.request.RequestPostUser;
import com.catering.assistent.model.request.UserEmailOnly;
import com.catering.assistent.model.response.ResponseGetUser;

import java.util.List;

import javax.validation.Valid;

public interface UserService {
    List<ResponseGetUser> returnAllUsersService();

    ResponseGetUser returnUserById(Long id) throws MyResourceNotFoundException;

    void deleteUserById(Long id) throws MyResourceNotFoundException;

    ResponseGetUser saveUser(@Valid RequestPostUser requestPostUser) throws DataMissingException, EmailExistentException;

    ResponseGetUser updateUser(Long id, @Valid RequestPostUser requestPostUser) throws DataMissingException,MyResourceNotFoundException;

	Object updateUserEmail(Long id, @Valid UserEmailOnly email) throws MyResourceNotFoundException, DataMissingException;
}
