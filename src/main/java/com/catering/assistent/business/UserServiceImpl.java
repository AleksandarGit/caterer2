package com.catering.assistent.business;

import com.catering.assistent.exception.DataMissingException;
import com.catering.assistent.exception.EmailExistentException;
import com.catering.assistent.exception.MyResourceNotFoundException;
import com.catering.assistent.model.User;
import com.catering.assistent.model.request.RequestPostUser;
import com.catering.assistent.model.request.UserEmailOnly;
import com.catering.assistent.model.response.ResponseGetUser;
import com.catering.assistent.repository.UserRepository;

import lombok.AllArgsConstructor;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import static com.catering.assistent.util.EmailValidation.validateEmailApache;

@Service
@Transactional
@AllArgsConstructor
public class UserServiceImpl implements UserService {

	private final UserRepository userRepository;

	@Override
	public ResponseGetUser returnUserById(Long id) throws MyResourceNotFoundException {
		Optional<User> userById = userRepository.findById(id);
		if (!userById.isPresent()) {
			throw new MyResourceNotFoundException();
		}
		ResponseGetUser responseGetUserById = new ResponseGetUser();
		responseGetUserById.setUserFirstName(userById.get().getFirstName());
		responseGetUserById.setUserLastName(userById.get().getLastName());
		responseGetUserById.setUserEmail(userById.get().getEmail());
		return responseGetUserById;
	}

	public ResponseGetUser returnUserByEmail(String email) throws MyResourceNotFoundException, DataMissingException {
		
		validateEmailApache(email);
		Optional<User> userByEmail = userRepository.findByEmail(email);
		
		ResponseGetUser responseGetUserByEmail = new ResponseGetUser();
		responseGetUserByEmail.setUserFirstName(userByEmail.get().getFirstName());
		responseGetUserByEmail.setUserLastName(userByEmail.get().getLastName());
		responseGetUserByEmail.setUserEmail(userByEmail.get().getEmail());
		return responseGetUserByEmail;
	}

	@Override
	public void deleteUserById(Long id) throws MyResourceNotFoundException {
		Optional<User> userById = userRepository.findById(id);
		if (!userById.isPresent()) {
			throw new MyResourceNotFoundException("User not found!", id);
		}
		userRepository.deleteById(id);
	}

	@Override
	public ResponseGetUser saveUser(@Valid RequestPostUser requestPostUser)
			throws DataMissingException, EmailExistentException {
		Optional<User> testuser = userRepository.findByEmail(requestPostUser.getUserEmail());
		if (testuser.isPresent()) {
			throw new EmailExistentException();
		}
			User user = new User();
		user.setFirstName(requestPostUser.getUserFirstName());
		user.setLastName(requestPostUser.getUserLastName());
		user.setEmail(requestPostUser.getUserEmail());

		userRepository.save(user);

		ResponseGetUser responseGetUser = new ResponseGetUser();
		responseGetUser.setUserEmail(user.getEmail());
		responseGetUser.setUserFirstName(user.getFirstName());
		responseGetUser.setUserLastName(user.getLastName());

		return responseGetUser;
	}

	@Override
	public ResponseGetUser updateUser(Long id, @Valid RequestPostUser requestPostUser)
			throws DataMissingException, MyResourceNotFoundException {

		Optional<User> userById = userRepository.findById(id);
		
		userById.get().setEmail(requestPostUser.getUserEmail());
		userById.get().setFirstName(requestPostUser.getUserFirstName());
		userById.get().setLastName(requestPostUser.getUserLastName());

		ResponseGetUser responseGetUser = new ResponseGetUser(userById.get());

		return responseGetUser;
	}

	@Override
	public List<ResponseGetUser> returnAllUsersService() {

		List<User> lista = (List<User>) userRepository.findAll();
		List<ResponseGetUser> responseUserList = new ArrayList<>();

		lista.forEach(item -> responseUserList.add(new ResponseGetUser(item)));

		return responseUserList;
	}

	@Override
	public ResponseGetUser updateUserEmail(Long id, @Valid UserEmailOnly email) throws MyResourceNotFoundException {
		Optional<User> userById = userRepository.findById(id);
		
		userById.get().setEmail(email.getUserEmail());
		ResponseGetUser responseGetUser = new ResponseGetUser(userById.get());
		return responseGetUser;
	}

}
