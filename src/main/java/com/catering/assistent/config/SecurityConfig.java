package com.catering.assistent.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.User.UserBuilder;
import org.springframework.security.crypto.password.NoOpPasswordEncoder;


@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

	@Override
	public void configure(AuthenticationManagerBuilder auth) throws Exception{    
	    auth.inMemoryAuthentication().passwordEncoder(NoOpPasswordEncoder.getInstance())
	            .withUser("sale").password("123").roles("ADMIN");
	}
		
		
	

	protected void configure(HttpSecurity http) throws Exception {
		http.httpBasic()
		.and().authorizeRequests().antMatchers("/**").hasRole("ADMIN")
		.and().authorizeRequests().antMatchers("/user/**", "/order/**").hasRole("MANAGER")
		.and().authorizeRequests().antMatchers("/order/**").hasRole("EMPLOYEE")
		.and().csrf().disable().headers().frameOptions().disable();
	}

}
