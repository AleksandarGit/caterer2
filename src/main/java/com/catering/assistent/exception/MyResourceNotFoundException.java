package com.catering.assistent.exception;

import lombok.Getter;

@Getter
public class MyResourceNotFoundException extends RuntimeException {
	/**
	 * This exception is thrown when there is no data in the database for that particular parameter!
	 *
	 * @param cause
	 * @param message
	 *
	 */
	private static final long serialVersionUID = 5185123181381L;
	private long resourceId;

	public MyResourceNotFoundException() {
		super();
	}

	public MyResourceNotFoundException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace, long resourceId) {
		super(message, cause, enableSuppression, writableStackTrace);
		this.resourceId= resourceId;
	}

	public MyResourceNotFoundException(String message, Throwable cause, long resourceId) {
		super(message, cause);
		this.resourceId= resourceId;
	}

	public MyResourceNotFoundException(String message, long resourceId) {
		super(message);
		this.resourceId= resourceId;
	}

	public MyResourceNotFoundException(String message) {
		super(message);
	}
	public MyResourceNotFoundException(Throwable cause, long resourceId) {
		super(cause);
		this.resourceId= resourceId;
	}

}
