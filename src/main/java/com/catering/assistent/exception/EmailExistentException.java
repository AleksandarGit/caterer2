package com.catering.assistent.exception;

import lombok.Getter;

@Getter
public class EmailExistentException extends RuntimeException {

	/**
	 * This exception is thrown when email exists in the database!
	 *
	 * @param cause
	 * @param message
	 *
	 */
	private static final long serialVersionUID = -2946595659456L;

	public EmailExistentException() {
		super();

	}

	public EmailExistentException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public EmailExistentException(String message, Throwable cause) {
		super(message, cause);
	}

	public EmailExistentException(String message) {
		super(message);
	}

	public EmailExistentException(Throwable cause) {
		super(cause);
	}

}
