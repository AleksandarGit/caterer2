package com.catering.assistent.exception;

import lombok.Getter;

@Getter
public class DataMissingException extends RuntimeException {

	/**
	 * This exception is thrown when request is not complete!
	 *
	 * @param cause
	 * @param message
	 *
	 */
	private static final long serialVersionUID = 7718828512143293558L;

	public DataMissingException(Throwable cause) {
		super(cause);
	}

	public DataMissingException() {
		super();
	}

	public DataMissingException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public DataMissingException(String message, Throwable cause) {
		super(message, cause);
	}

	public DataMissingException(String message) {
		super(message);
	}

}
