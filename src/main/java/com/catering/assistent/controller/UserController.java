package com.catering.assistent.controller;

import com.catering.assistent.business.UserServiceImpl;
import com.catering.assistent.exception.DataMissingException;
import com.catering.assistent.exception.EmailExistentException;
import com.catering.assistent.exception.MyResourceNotFoundException;
import com.catering.assistent.model.request.RequestPostUser;
import com.catering.assistent.model.request.UserEmailOnly;
import com.catering.assistent.model.response.ResponseGetUser;

import lombok.AllArgsConstructor;
import lombok.NonNull;

import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;

import javax.validation.Valid;
import org.springframework.web.bind.annotation.*;
import java.util.List;

@RestController
@Transactional
@RequestMapping("/user")
@AllArgsConstructor
public class UserController {

	private final @NonNull UserServiceImpl userServiceImpl;

	@GetMapping("/{id}")
	public ResponseEntity<ResponseGetUser> returnUserByIdController(@PathVariable("id") Long id)
			throws MyResourceNotFoundException {
		return ResponseEntity.ok(userServiceImpl.returnUserById(id));
	}

	@GetMapping("/{email}")
	public ResponseEntity<ResponseGetUser> returnUserByEmailController(@PathVariable("email") String email)
			throws MyResourceNotFoundException, DataMissingException {
		return ResponseEntity.ok(userServiceImpl.returnUserByEmail(email));
	}

	@GetMapping("/all")
	public ResponseEntity<List<ResponseGetUser>> returnAllUsers() {
		return ResponseEntity.ok(userServiceImpl.returnAllUsersService());
	}

	@PostMapping("/")
	public ResponseEntity<ResponseGetUser> postNewUser(@RequestBody @Valid RequestPostUser userRequest)
			throws DataMissingException, EmailExistentException {
		return ResponseEntity.ok(userServiceImpl.saveUser(userRequest));
	}

	@DeleteMapping("/{id}")
	public void deleteUser(@PathVariable("id") Long id) throws MyResourceNotFoundException {
		userServiceImpl.deleteUserById(id);
	}

	@PutMapping("/{id}")
	public ResponseEntity<ResponseGetUser> updateUser(@PathVariable("id") Long id,
			@RequestBody @Valid RequestPostUser userRequest) throws DataMissingException, MyResourceNotFoundException {
		return ResponseEntity.ok(userServiceImpl.updateUser(id, userRequest));
	}

	@PatchMapping("/{id}")
	public ResponseEntity<ResponseGetUser> partialUpdateName(@RequestBody @Valid UserEmailOnly email,
			@PathVariable("id") Long id) throws MyResourceNotFoundException, DataMissingException {
		return ResponseEntity.ok(userServiceImpl.updateUserEmail(id, email));
	}
}
