package com.catering.assistent.controller;

import com.catering.assistent.business.OrderServiceImpl;
import com.catering.assistent.model.request.RequestPostOrder;
import com.catering.assistent.model.response.ResponseGetAllOrders;
import com.catering.assistent.model.response.ResponseGetAllOrdersByEmail;
import com.catering.assistent.model.response.ResponseGetOrder;

import lombok.AllArgsConstructor;
import lombok.NonNull;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import javax.validation.Valid;

import java.net.URI;
import java.util.List;

@RestController
@Transactional
@RequestMapping("/order")
@AllArgsConstructor
public class OrderController {

	private final @NonNull OrderServiceImpl orderServiceImpl;

	@GetMapping("/{id}")
	public ResponseGetOrder returnOrderById(@PathVariable("id") long id) {
		return orderServiceImpl.returnOrderById(id);
	}

	@GetMapping("/all")
	public List<ResponseGetOrder> returnAllOrders() {
		return orderServiceImpl.returnAllOrderService();
	}

	@GetMapping(value = "/", params = "email")
	public ResponseGetAllOrdersByEmail returnAllOrdersByEmail(@RequestParam("email") String email) {
		return orderServiceImpl.returnOrderByUserEmail(email);
	}

	@GetMapping(value = "/", params = "date")
	public ResponseGetAllOrders returnAllOrdersByDate(
			@RequestParam("date") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) String date) {
		return orderServiceImpl.returnOrderByDate(date);
	}

	@GetMapping(value = "/", params = "meal")
	public ResponseGetAllOrders returnAllOrdersByMeal(@RequestParam("meal") String meal) {
		return orderServiceImpl.returnOrderByMeal(meal);
	}

	@PostMapping(value = "/", consumes = "application/json")
	public ResponseEntity<ResponseGetOrder> postNewOrder(@RequestBody @Valid RequestPostOrder orderRequest,
			UriComponentsBuilder ucb) {
		ResponseGetOrder responseGetOrder = orderServiceImpl.saveOrder(orderRequest);
		HttpHeaders headers = new HttpHeaders();
		URI locationUri = ucb.path("/order/").path(String.valueOf(responseGetOrder.getId())).build().toUri();
		headers.setLocation(locationUri);
		ResponseEntity<ResponseGetOrder> responseEntity = new ResponseEntity<>(responseGetOrder, headers,
				HttpStatus.CREATED);
		return responseEntity;
	}

	@PostMapping(value = "/", consumes = "application/json", params = "date")
	public ResponseEntity<ResponseGetOrder> postNewOrderWithDate(
			@RequestParam("date") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) String date,
			@RequestBody @Valid RequestPostOrder orderRequest, UriComponentsBuilder ucb) {
		ResponseGetOrder responseGetOrder = orderServiceImpl.saveOrderWithDate(date, orderRequest);
		HttpHeaders headers = new HttpHeaders();
		URI locationUri = ucb.path("/order/").path(String.valueOf(responseGetOrder.getId())).build().toUri();
		headers.setLocation(locationUri);
		ResponseEntity<ResponseGetOrder> responseEntity = new ResponseEntity<>(responseGetOrder, headers,
				HttpStatus.CREATED);
		return responseEntity;
	}

	@DeleteMapping("/{id}")
	public void deleteOrder(@PathVariable("id") Long id) {
		orderServiceImpl.deleteOrderById(id);
	}

	@PutMapping("/{id}")
	public ResponseEntity<ResponseGetOrder> updateOrder(@PathVariable("id") Long id,
			@RequestBody @Valid RequestPostOrder orderRequest, UriComponentsBuilder ucb) {
		ResponseGetOrder responseGetOrder = orderServiceImpl.updateOrder(id, orderRequest);
		HttpHeaders headers = new HttpHeaders();
		URI locationUri = ucb.path("/order/").path(String.valueOf(responseGetOrder.getId())).build().toUri();
		headers.setLocation(locationUri);
		ResponseEntity<ResponseGetOrder> responseEntity = new ResponseEntity<>(responseGetOrder, headers,
				HttpStatus.CREATED);
		return responseEntity;
	}
}
