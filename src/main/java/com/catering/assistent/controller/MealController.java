package com.catering.assistent.controller;

import com.catering.assistent.business.MealServiceImpl;
import com.catering.assistent.model.request.RequestPostMeal;
import com.catering.assistent.model.response.ResponseGetMeal;

import lombok.AllArgsConstructor;
import lombok.NonNull;

import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;
import java.util.List;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;

@RestController
@Transactional
@RequestMapping("/meal")
@AllArgsConstructor
public class MealController {
   
    private final @NonNull MealServiceImpl mealServiceImpl;
    
    @GetMapping("/{id}")
    public ResponseEntity<ResponseGetMeal> returnMealById(@PathVariable("id") Long id){
        return ResponseEntity.ok(mealServiceImpl.returnMealById(id));
    }
    @GetMapping("/")
    public ResponseEntity<ResponseGetMeal> returnMealById(@RequestParam ("mealName") String mealName){
        return ResponseEntity.ok(mealServiceImpl.returnMealByMealName(mealName));
    }

    @GetMapping("/all")
    public ResponseEntity<List<ResponseGetMeal>> returnAllMeals() {
        return ResponseEntity.ok(mealServiceImpl.returnAllMealsService());
    }


    @PostMapping("/")
    public ResponseEntity<ResponseGetMeal> postNewMeal(@RequestBody RequestPostMeal mealRequest){
        return ResponseEntity.ok(mealServiceImpl.saveMeal(mealRequest));
    }

    @DeleteMapping("/{id}")
    public void deleteMeal(@PathVariable("id") Long id){
        mealServiceImpl.deleteMealById(id);
    }

    @PutMapping("/{id}")
    public ResponseEntity<ResponseGetMeal> updateMeal(@PathVariable("id") Long id, @RequestBody @Valid RequestPostMeal mealRequest){
        return ResponseEntity.ok(mealServiceImpl.updateMeal(id, mealRequest));
    }
    
}
